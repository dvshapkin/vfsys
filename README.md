# Virtual file system

[![Crates.io](https://img.shields.io/crates/l/vfsys)](https://gitlub.com/dvshapkin/vfsys/LICENSE)

For convenient work with relative paths.

<small>Tested on linux, windows.</small>
